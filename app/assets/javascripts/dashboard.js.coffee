# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

showDescription = ->
  $("#description").fadeIn()


hideDescription = ->
  $("#description").fadeOut()

$ ->
  $("#description").hide()
  $('#average').mouseover -> showDescription()
  $('#average').mouseleave -> hideDescription()

